#!/usr/bin/env python3
import logging
import os
import subprocess

import send_data
from apscheduler.schedulers.blocking import BlockingScheduler

import file_handler


def main() -> None:
    """main function"""
    logging.info("Starting the main process...")
    path = (
        os.path.dirname(
            os.path.join(os.path.dirname(os.path.realpath(__file__)))
        )
        + "/make-lockfile/make-lockfile.py"
    )

    logging.info(
        f"Calling make-lockfile submodule \n\npython3 {path}"
        " --outfile cs9-image-manifest.lock.json"
    )
    proc = subprocess.Popen(
        ["python3", path, "--outfile", "cs9-image-manifest.lock.json"]
    )
    try:
        outs_, errs_ = proc.communicate(timeout=100)
    except subprocess.TimeoutExpired:
        logging.error("command to make-lockfile timeouts")
        proc.kill()
        outs_, errs_ = proc.communicate()

    logging.debug("Successfully created file cs9-image-manifest.lock.json")
    f_handler = file_handler.FileHandler("cs9-image-manifest.lock.json")

    logging.info("Parse and diff...")
    result, added_nvr_list, nvrs_removed = f_handler.parse_and_diff()
    if result:
        logging.info("Parse and diff successful. Starting to send the data\n")
        sender = send_data.SendData(added_nvr_list, nvrs_removed)
        sender.send_parameters(
            f_handler.collect_final_data(),
            "cs9-image-manifest.lock.json",
        )
        logging.info("Sent parameters\n")
    else:
        logging.error("Error ocurred. No data to send")


if __name__ == "__main__":
    scheduler = BlockingScheduler()
    scheduler.add_job(main, "interval", hours=24)
    scheduler.start()

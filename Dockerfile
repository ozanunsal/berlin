FROM registry.fedoraproject.org/fedora:34

RUN dnf -y install fedora-messaging \
    python3-pip \
    make \
    poetry \
    &&  mkdir /home/berlin

WORKDIR /home/berlin

COPY . /home/berlin

RUN poetry config virtualenvs.create false \
    && poetry install \
    && rm -rf ~/.cache

ENTRYPOINT [ "/bin/sh" ]

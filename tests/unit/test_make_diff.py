import unittest

import make_diff
import pytest


@pytest.mark.usefixtures("nvrs")
class TestDiff(unittest.TestCase):
    def test_diff(self):
        make_diff_obj = make_diff.Diff()
        (
            list_changed,
            result_nvr_list,
            additional_nvr_list,
            removed_pkg,
        ) = make_diff_obj.diff(self.repo_nvrs, self.generated_nvrs)
        result_nvr_list.sort()
        additional_nvr_list.sort()
        self.assertEqual(result_nvr_list, self.result_nvr_list)
        self.assertEqual(additional_nvr_list, self.additional_nvr_list)

        self.assertTrue(list_changed)
        self.assertTrue(removed_pkg)

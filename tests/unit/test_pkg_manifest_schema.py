import json
import unittest

import pkg_manifest_schema
import pytest


@pytest.mark.usefixtures("json_data")
class TestPkgManifestSchema(unittest.TestCase):
    def test_serialize(self):
        test_obj = pkg_manifest_schema.PackageManifestSchema()
        manifest_data = json.loads(self.json_file_data)
        self.assertEqual(len(test_obj.common), 0)
        test_obj.deserialize(manifest_data)
        self.assertEqual(len(test_obj.common), 1)

    def test_deserialize(self):
        test_obj = pkg_manifest_schema.PackageManifestSchema()
        test_obj.deserialize(json.loads(self.json_file_data))
        py_data = test_obj.serialize()
        file_data_str: str = json.dumps(py_data, separators=(",", ":"))
        self.assertEqual(file_data_str, self.json_file_data)
